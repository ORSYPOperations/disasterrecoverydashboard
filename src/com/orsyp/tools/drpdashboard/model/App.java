package com.orsyp.tools.drpdashboard.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class App implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public String applicationName;
	public String sessionFO;
	public String sessionFB;	
	public String duration;
	public String lastDate;
	public String lastTime;
	
	public String joblogUproc;
	public int joblogLine = 0;
	
	public List<String> nodesFO = new ArrayList<String>();
	public List<String> nodesFB = new ArrayList<String>();
	
	transient public DRPItem data;
	
	public String getStringList(List<String> list) {
		String ret = "";
		for (String s:list) {
			if (ret.length()>0)
				ret+=", ";
			ret+=s;
		}
		return ret;
			
	}
}
