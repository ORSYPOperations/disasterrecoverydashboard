package com.orsyp.tools.drpdashboard.model;

import java.io.Serializable;

public class Uvms implements Serializable{
	private static final long serialVersionUID = 1L;
	
	public String url;
	public String uvmsHost;
	public int uvmsPort;
	public String uvmsLogin;
	public String uvmsPass;
	public String area;
	public String company;
	public String refNodeName;
}
