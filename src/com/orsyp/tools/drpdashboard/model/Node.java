package com.orsyp.tools.drpdashboard.model;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.orsyp.duws.Context;
import com.orsyp.duws.ContextHolder;
import com.orsyp.duws.DuWebService_PortType;
import com.orsyp.duws.DuWebService_ServiceLocator;
import com.orsyp.duws.DuwsException;
import com.orsyp.duws.Envir;
import com.orsyp.duws.UvmsContext;
import com.orsyp.tools.drpdashboard.connection.Connection;

public class Node {	
	
	private Connection connection;
	private String node;
	private Uvms uvms;
	public boolean isReferenceNode = false;
	
	@SuppressWarnings("unused")
	private Node() {}
	
	public Node (Uvms uvms, String node) {
		this.uvms = uvms;
		this.node = node;
	}
	
	private Connection createConnection() throws ServiceException, DuwsException, RemoteException {
		DuWebService_ServiceLocator ss = new DuWebService_ServiceLocator();
		ss.setDuWebServicePortAddress(uvms.url);
		DuWebService_PortType service = ss.getDuWebServicePort();
		UvmsContext uvmsContext = new UvmsContext();
		uvmsContext.setUvmsHost(uvms.uvmsHost);
		uvmsContext.setUvmsPort(uvms.uvmsPort);
		uvmsContext.setUvmsUser(uvms.uvmsLogin);
		uvmsContext.setUvmsPassword(uvms.uvmsPass);
		//	Invoking authentication
		String token = service.login(uvmsContext);
		Envir envir = new Envir(uvms.company,node,uvms.area,null,"");
		//	Setting up the context
		Context context = new Context(envir);
		ContextHolder ctxHolder = new ContextHolder(token, context);		
		return new Connection(service, ctxHolder,token);				
	}

	public Connection getConnection() throws Exception {
		if (connection==null)
			connection=createConnection();
		return connection;
	}
}

