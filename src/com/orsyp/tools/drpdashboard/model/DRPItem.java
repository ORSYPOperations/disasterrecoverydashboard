package com.orsyp.tools.drpdashboard.model;

import java.util.List;

public class DRPItem implements Comparable<DRPItem> {	
	public String applicationName;
	
	public List<String> uprocsFO;
	public List<String> uprocsFB;
	
	public int totalCompletedFO;
	public int totalCompletedFB;
	public int percentagesFO;
	public int percentagesFB;
	public String durationFO;
	public String durationFB;
	public String drillDateFO;
	public String drillDateFB;
	public boolean drReadiness;

	public String rpoStatus;
	
	public int joblogLine;
	public int nodeId;
	
	@Override
	public int compareTo(DRPItem obj) {
		return applicationName.compareTo(((DRPItem)obj).applicationName);
	}	

}
