package com.orsyp.tools.drpdashboard.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.orsyp.tools.drpdashboard.AppManager;
import com.orsyp.tools.drpdashboard.connection.NodeManager;

/**
 * Servlet implementation class ConfigCheck
 */
public class ConfigCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConfigCheck() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (AppManager.getData().size()>0 && NodeManager.getCurrentUvms()!=null)
			response.sendRedirect("dashboard.jsp");
		else
			response.sendRedirect("admin/admin.jsp");
	}

}
