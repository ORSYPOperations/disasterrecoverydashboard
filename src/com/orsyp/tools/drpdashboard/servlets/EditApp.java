package com.orsyp.tools.drpdashboard.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.orsyp.tools.drpdashboard.AppManager;
import com.orsyp.tools.drpdashboard.model.App;

/**
 * Servlet implementation class EditApp
 */
public class EditApp extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditApp() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		App app = new App();
		app.applicationName = request.getParameter("name");
		app.duration = request.getParameter("duration");
		if (request.getParameter("joblogline").length()>0)
			app.joblogLine = Integer.parseInt(request.getParameter("joblogline"));		
		app.joblogUproc = request.getParameter("jobloguproc");
		app.sessionFO = request.getParameter("fosession");
		app.sessionFB = request.getParameter("fbsession");
		
		app.nodesFO = parseNodeList(request.getParameter("fonodes"));
		app.nodesFB = parseNodeList(request.getParameter("fbnodes"));
		
		AppManager.removeApp(app.applicationName);
		AppManager.addApp(app);
		AppManager.saveApps();
		response.sendRedirect("admin/admin.jsp");
	}

	private List<String> parseNodeList(String str) { 
		List<String> l = new ArrayList<String>();
		for (String tk : str.split(",")) 
			if (tk.trim().length()>0)
				l.add(tk.trim());
		return l;
	}

}
