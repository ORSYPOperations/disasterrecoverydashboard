package com.orsyp.tools.drpdashboard.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.orsyp.tools.drpdashboard.connection.NodeManager;
import com.orsyp.tools.drpdashboard.model.Uvms;

/**
 * Servlet implementation class SaveUvms
 */
public class SaveUvms extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SaveUvms() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Uvms u = new Uvms();
		u.area = request.getParameter("area");
		u.company = request.getParameter("company");
		u.refNodeName = request.getParameter("node");
		u.url = request.getParameter("url");
		u.uvmsHost = request.getParameter("server");
		u.uvmsLogin = request.getParameter("user");
		u.uvmsPass = request.getParameter("password");
		u.uvmsPort = Integer.parseInt(request.getParameter("port"));
		NodeManager.saveConf(u,getServletContext());
		response.sendRedirect("admin/admin.jsp");
	}

}
