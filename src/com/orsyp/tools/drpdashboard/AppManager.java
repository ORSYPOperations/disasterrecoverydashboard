package com.orsyp.tools.drpdashboard;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.List;
import java.util.TreeMap;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.servlet.ServletContext;

import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;

import com.orsyp.duws.ExecutionItem;
import com.orsyp.tools.drpdashboard.connection.Connection;
import com.orsyp.tools.drpdashboard.connection.NodeManager;
import com.orsyp.tools.drpdashboard.model.App;
import com.orsyp.tools.drpdashboard.model.DRPItem;

public class AppManager {
	
	private static TreeMap<String,App> apps = new TreeMap<String,App>();
	private static ServletContext ctx;
	private static DateTime lastRead;
	
	public static Collection<App> getData() {
		try {
			if (lastRead==null || lastRead.isBefore(DateTime.now().minusSeconds(60)))
				readData();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return apps.values();
	} 
	
	public static void setDataItem(DRPItem item) {				
		App app = apps.get(item.applicationName);
		if (app!=null)
			app.data=item;
	}
	
	public static DRPItem getDataItem(String appName) {
		return apps.get(appName).data;
	}

	public static void addApp(App app) {
		apps.put(app.applicationName,app);
	} 

	
	public static void initApps(ServletContext ctx) throws NumberFormatException, IOException {
		AppManager.ctx=ctx;
		
		apps=loadSerializedApps();
		if (apps==null)
			apps=new TreeMap<String, App>();
		
		/*
		apps.clear();
		//read application config data from csv file
		CSVReader reader = new CSVReader(new FileReader(ctx.getRealPath("")+File.separator+"WEB-INF/config/apps.conf"),',', '\"', '\0');
		String [] line;
	    while ((line = reader.readNext()) != null) {	
    		if (!line[0].startsWith("#")) {
    			//Application name, Fail over filter, Fail back filter, Duration, Last Date, Last Time, Job log line number
    			App app = new App(); 
    			app.applicationName = line[0];
    			app.sessionFO = line[1];
    			app.sessionFB = line[2];
    			app.duration = line[3];
    			app.lastDate = line[4];
    			app.lastTime = line[5];
    			if (line.length>6) { 
    				if (line[6].length()>0) {
    					app.joblogUproc = line[6];
	    				if (line.length>7)
	        				app.joblogLine = Integer.parseInt(line[7]);
    				}
    			}    			
    			
    			int idx=8;
    			while (line.length>idx) {
    				if (line[idx].startsWith("FO:"))
    					app.nodesFO.add(line[idx].replace("FO:", ""));
    				else
					if (line[idx].startsWith("FB:"))
    					app.nodesFB.add(line[idx].replace("FB:", ""));
    				
    				idx++;
    			}
    			
    			addApp(app);
    		}
	    }
	    */		
	}
	
	
	
	public static void readData() throws IOException {
		System.out.println("Reading data...");
		lastRead = DateTime.now();
		for (App app : apps.values()) {
			Connection conn;
			try {
				conn = NodeManager.getRefNode(ctx).getConnection();
				DRPItem item = AppManager.getDataItem(app.applicationName);
				
				//first time setup
				if (item==null) {
					item = new DRPItem();
					item.applicationName = app.applicationName;				
					item.uprocsFO = conn.getSessionUProcs(app.sessionFO);
					item.uprocsFB = conn.getSessionUProcs(app.sessionFB);					
					
					item.drReadiness = item.uprocsFO.size() != 0 || item.uprocsFB.size() != 0;
					
					item.percentagesFO=0;
					item.percentagesFB=0;
					item.durationFO="N/A";
					item.durationFB="N/A";
					item.drillDateFO="N/A";
					item.drillDateFB="N/A";
				}
				
				
				if (item.drReadiness) {
					
					item.totalCompletedFO=0;
					for (String node: app.nodesFO) {
						Connection c = NodeManager.getNode(ctx, node).getConnection();
						
						List<ExecutionItem> list = c.getLastExecutions(app.sessionFO);
						item.totalCompletedFO += list.size();	
						
						DateTime firstStart = getFirstStart(list);
						DateTime lastEnd = DateTime.now();
						DateTime lastDrill = null;
						if (item.uprocsFO.size()==list.size()) {
							lastEnd = getLastEnd(list);
							lastDrill = new DateTime(lastEnd);
						}
						
						if (item.uprocsFO.size()>0) {
							long perc = 100 * item.totalCompletedFO / item.uprocsFO.size();
							item.percentagesFO= Math.round(perc);
						}
						
						if (item.percentagesFO>0) {
							int mins = Minutes.minutesBetween(firstStart, lastEnd).getMinutes();
							if (mins>0)
								item.durationFO = String.valueOf(mins) + " mins";
							else {
								int secs = Seconds.secondsBetween(firstStart, lastEnd).getSeconds();
								item.durationFO = String.valueOf(secs) + " secs";
							}
						}
						else
							item.durationFO="N/A";
						
						if (item.percentagesFO == 0 || item.percentagesFO == 100) {
							if (lastDrill!=null)
								item.drillDateFO=lastDrill.toString(DateTimeFormat.forPattern("dd/MM/yyyy"));
							else
								item.drillDateFO="N/A";
						}
						else 
							item.drillDateFO="In Progress Now";
					}
					
					item.totalCompletedFB=0;
					for (String node: app.nodesFB) {
						Connection c = NodeManager.getNode(ctx, node).getConnection();
						
						List<ExecutionItem> list = c.getLastExecutions(app.sessionFB);
						item.totalCompletedFB += list.size();	
						
						DateTime firstStart = getFirstStart(list);
						DateTime lastEnd = DateTime.now();
						DateTime lastDrill = null;
						if (item.uprocsFB.size()==list.size()) {
							lastEnd = getLastEnd(list);
							lastDrill = new DateTime(lastEnd);
						}
						
						if (item.uprocsFB.size()>0) {
							long perc = 100 * item.totalCompletedFB / item.uprocsFB.size();
							item.percentagesFB= Math.round(perc);
						}
						
						if (item.percentagesFB>0) {
							int mins = Minutes.minutesBetween(firstStart, lastEnd).getMinutes();
							if (mins>0)
								item.durationFB = String.valueOf(mins) + " mins";
							else {
								int secs = Seconds.secondsBetween(firstStart, lastEnd).getSeconds();
								item.durationFB = String.valueOf(secs) + " secs";
							}
						}
						else
							item.durationFB="N/A";
						
						if (item.percentagesFB == 0 || item.percentagesFB == 100) {
							if (lastDrill!=null)
								item.drillDateFB=lastDrill.toString(DateTimeFormat.forPattern("dd/MM/yyyy"));
							else
								item.drillDateFB="N/A";
						}
						else 
							item.drillDateFB="In Progress Now";
					}					
				
					
					if (app.joblogUproc!=null)
						try {
							item.rpoStatus=conn.getLogs(conn.getIds(conn.getExecutions(app.joblogUproc)))[app.joblogLine];
						} catch (Exception e ) {}
					else
						item.rpoStatus="N/A";				
				}
				
				if (item!=null)
					AppManager.setDataItem(item);
				lastRead = DateTime.now();
				System.out.println("Done");
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Error reading data");
			}			
		}		
	}

	private static DateTime getLastEnd(List<ExecutionItem> list) {		
		DateTime start = getFirstStart(list);
		DateTime end = new DateTime(start);
		for (ExecutionItem it: list) {
			DateTime t = getDateTime(it.getEndDate(), it.getEndHour());
			if (t.isAfter(end))
				end= t;
		}
		return end;
	}

	private static DateTime getDateTime(String endDate, String endHour) {		
		return DateTime.parse(endDate+endHour, DateTimeFormat.forPattern("yyyyMMddHHmmss"));
	}

	private static DateTime getFirstStart(List<ExecutionItem> list) {
		DateTime start = DateTime.now();
		for (ExecutionItem it: list) {
			DateTime t = getDateTime(it.getBeginDate(), it.getBeginHour());
			if (t.isBefore(start))
				start = t;
		}
		return start;
	}

	@SuppressWarnings("unchecked")
	private static TreeMap<String,App> loadSerializedApps() {
		String filename = ctx.getRealPath("")+File.separator+"WEB-INF/config/apps";
		if (!(new File(filename).exists())) {
			System.out.println("App config file not found: "+filename);
			return null;
		}
		System.out.println("Loading App config file '"+filename+"' ...");
		try {
			FileInputStream fos = new FileInputStream(filename);
			GZIPInputStream gz = new GZIPInputStream(fos);
			BufferedInputStream buf = new BufferedInputStream(gz);
			
			ObjectInputStream oos = new ObjectInputStream(buf);			
			return (TreeMap<String,App>) oos.readObject();					
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return null;		
	} 
	
	public static void saveApps() {
		String filename = ctx.getRealPath("")+File.separator+"WEB-INF/config/apps";
		try {
			FileOutputStream fo = new FileOutputStream( new File(filename));
			GZIPOutputStream gz = new GZIPOutputStream(fo);					   
			BufferedOutputStream bf = new BufferedOutputStream(gz);
			ObjectOutputStream oout = new ObjectOutputStream(bf);			
			oout.writeObject(apps);
			oout.close();
		} catch (Exception e) {
			e.printStackTrace();			
		}	
	}

	public static void removeApp(String appname) {
		if (appname!=null)
			apps.remove(appname);
		
	}
}
