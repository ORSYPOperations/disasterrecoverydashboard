package com.orsyp.tools.drpdashboard.background;

import java.io.IOException;
import javax.servlet.ServletContext;

import com.orsyp.tools.drpdashboard.AppManager;

public class ReaderThread extends Thread {

	private ServletContext servletContext;
	
	public ReaderThread(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	@Override
	public void run() {
		try {
			AppManager.initApps(servletContext);
			AppManager.readData();
		} catch (IOException e) {
			e.printStackTrace();
		}
		/*
		while (!interrupted()) {
			try {
				readData();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			try {
				sleep(60000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		*/
	}
}
