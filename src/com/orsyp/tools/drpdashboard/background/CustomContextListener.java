package com.orsyp.tools.drpdashboard.background;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class CustomContextListener  implements ServletContextListener {

	private ReaderThread thread;
	
	@Override
	public void contextDestroyed(ServletContextEvent evt) {
		if (thread!=null) {
			thread.interrupt();
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}		
	}

	@Override
	public void contextInitialized(ServletContextEvent evt) {
		thread = new ReaderThread(evt.getServletContext());
		thread.start();		
	}
}