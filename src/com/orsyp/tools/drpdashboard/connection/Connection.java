package com.orsyp.tools.drpdashboard.connection;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.net.telnet.TelnetClient;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Seconds;

import com.orsyp.duws.*;

public class Connection {
	
	private ContextHolder ctxHolder;
	private DuWebService_PortType service;
	
	public Connection(DuWebService_PortType service, ContextHolder ctxHolder, String token) {
		this.ctxHolder = ctxHolder;
		this.service = service;
	}


	public ExecutionItem[] getExecutions(String uprocFilter) {
		ExecutionItem[] execList = null;
		ExecutionFilter filterCompleted = new ExecutionFilter();
		
		filterCompleted.setUproc(uprocFilter);

		try {
			execList=service.getListExecution(ctxHolder, filterCompleted);
			return execList;
		}
		catch (Exception e)	{
			//e.printStackTrace();
			return null;
		}
		
	}
	
	public ExecutionId getIds(ExecutionItem[] execList) {
		try {
			return execList[execList.length-1].getIdent();	 
		}
		catch (Exception e)	{
			//e.printStackTrace();
			return null;
		}		
	}
	
	public String[] getLogs(ExecutionId executionId) {
		ExecutionLog executionLog = null;

		try {
			executionLog=service.getExecutionLog(ctxHolder, executionId);
			return executionLog.getLog();
		}
		catch (Exception e)	{
			//e.printStackTrace();
			return null;
		}
		
	}
	
	public int getNumberUprocsAll(String uprocFilter) {
		UprocItem[] uprocListAll = null;
		UprocFilter filterAll = new UprocFilter();
		
		filterAll.setUproc(uprocFilter);

		try {
			uprocListAll=service.getListUproc(ctxHolder, filterAll);
		}
		catch (Exception e)	{
			//e.printStackTrace();
		}
		
		if (uprocListAll == null) {
			return 0;
		}
		else {
			return uprocListAll.length;
		}
	}
	
	public int countFileLines(String filename) throws IOException {
	    InputStream is = new BufferedInputStream(new FileInputStream(filename));
	    try {
	        byte[] c = new byte[1024];
	        int count = 0;
	        int readChars = 0;
	        boolean empty = true;
	        while ((readChars = is.read(c)) != -1) {
	            empty = false;
	            for (int i = 0; i < readChars; ++i) {
	                if (c[i] == '\n') {
	                    ++count;
	                }
	            }
	        }
	        return (count == 0 && !empty) ? 1 : count;
	    } 
	    finally {
	        is.close();
	    }
	}
	

	public ExecutionItem[] getExecutions(String uprocFilter, String statusFilter, String dateFilter, String timeFilter) {
		ExecutionItem[] execList = null;
		ExecutionFilter filterCompleted = new ExecutionFilter();
		
		filterCompleted.setUproc(uprocFilter);
		filterCompleted.setStatus(statusFilter);
		filterCompleted.setBeginDateMin(dateFilter);
		filterCompleted.setBeginHourMin(timeFilter);

		try {
			execList=service.getListExecution(ctxHolder, filterCompleted);
			return execList;
		}
		catch (Exception e)	{
			//e.printStackTrace();
			return null;
		}
		

	}
	
	
	public int getNumberExecutionCompleted(String uprocFilter, String statusFilter, String dateFilter, String timeFilter) {
		ExecutionItem[] execListCompleted = getExecutions(uprocFilter,statusFilter, dateFilter, timeFilter);
		
		if (execListCompleted == null) {
			return 0;
		}
		else { 
			return execListCompleted.length;
		}
	}
	
	
	public int getExecutionsPercentage(String uprocFilter,String dateFilter, String timeFilter){
		int all=getNumberSteps(uprocFilter);
		int completed=getNumberExecutionCompleted(uprocFilter,"COMPLETED", dateFilter, timeFilter);
		int percentage = (int)(((double)completed / all) * 100);
		return percentage;
	}
	
	public int getExecutionsCount(String uprocFilter,String dateFilter, String timeFilter){
		return getNumberExecutionCompleted(uprocFilter,"COMPLETED", dateFilter, timeFilter);
	}

	public int getNumberSteps(String uprocFilter){
		int all=getNumberUprocsAll(uprocFilter);
		return all;
	}
	
	public String getDRReadilyness(String DRSite, int port) {
		try {
			TelnetClient tc = new TelnetClient();
			tc.connect(DRSite, port);
			tc.disconnect();
			return "Ready";
		} 
		catch (SocketException e) {
			//e.printStackTrace();
			return "Impaired";
		} 
		catch (IOException e) {
			//e.printStackTrace();
			return "Impaired";
		}
	}
	
	public DateTime getExecutionTime(String uprocFilter, String status, String dateFilter, String timeFilter) {
		ExecutionItem[] execListCompleted = getExecutions(uprocFilter,status,dateFilter,timeFilter);
		
		if (execListCompleted == null) {
			//System.out.println("[WARN] Null value during getting execution times");
			return null;
		}
		else {
			int runYear=Integer.parseInt(execListCompleted[execListCompleted.length-1].getBeginDate().substring(0, 4));
			int runMonth=Integer.parseInt(execListCompleted[execListCompleted.length-1].getBeginDate().substring(4, 6));
			int runDay=Integer.parseInt(execListCompleted[execListCompleted.length-1].getBeginDate().substring(6, 8));
			int runHour=Integer.parseInt(execListCompleted[execListCompleted.length-1].getBeginHour().substring(0, 2));
			int runMinute=Integer.parseInt(execListCompleted[execListCompleted.length-1].getBeginHour().substring(2, 4));
			int runSecond=Integer.parseInt(execListCompleted[execListCompleted.length-1].getBeginHour().substring(4, 6));
			
			DateTime dt = new DateTime(runYear, runMonth, runDay, runHour, runMinute, runSecond, 0);
			return dt;
		}

	}
	
	public String getTimeDifference(String header, String trailer, String dateFilter, String timeFilter) throws IOException {
		    DateTime dt1= getExecutionTime(header,"COMPLETED",dateFilter,timeFilter);
		    DateTime dt2= getExecutionTime(trailer,"COMPLETED",dateFilter,timeFilter);
			String difference="";

		    if (dt1 == null) {
		    	return "N/A";
		    }
		    if (dt2== null) {
		    	dt2 = new DateTime();
		        difference=difference + "In Progress: ";
		    }
		    
			//Logger.log(dt1.toString());
			//Logger.log(dt2.toString());
		    int seconds= Seconds.secondsBetween(dt1, dt2).getSeconds();
		    if (seconds >= 60) { seconds=seconds%60; }
			int minutes = Minutes.minutesBetween(dt1, dt2).getMinutes();
			if (minutes >= 60) { minutes=minutes%60; }
		    int hours = Hours.hoursBetween(dt1, dt2).getHours();
		    if (hours >= 24) { hours=hours%24; }
			int days = Days.daysBetween(dt1, dt2).getDays();

			if (days < 0 || hours < 0 || minutes < 0) {
				dt2 = new DateTime();
				dt2 = dt2.plusHours(3); // UTC+4 fixation
				seconds= Seconds.secondsBetween(dt1, dt2).getSeconds();
				minutes = Minutes.minutesBetween(dt1, dt2).getMinutes();
				hours = Hours.hoursBetween(dt1, dt2).getHours();
				days = Days.daysBetween(dt1, dt2).getDays();
				if (minutes >= 60) { minutes=minutes%60; }
				if (hours >= 24) { hours=hours%24; }
				
				//difference=difference + "In Progress: ";
			}

			if (days != 0) { 
				difference=difference + days + " day(s) "; 
			}
			
			if (hours != 0) {
				difference=difference + hours + " hour(s) ";
			}
			
			if (minutes != 0) {
				difference=difference + minutes + " minute(s)"; 
			}
			if (seconds != 0 && minutes == 0 && hours == 0 && days == 0) {
				return "~ 1 minute"; 
			}
				
			if (difference.equals("")) { 
				return "N/A";
			} 
			else {
				return difference;
			}
	}
	
	
	public String getLastDrill(String header, String trailer, String dateFilter, String timeFilter){
		ExecutionItem[] headerCompleted = getExecutions(header,"*",dateFilter,timeFilter);
		ExecutionItem[] trailerCompleted = getExecutions(trailer,"*",dateFilter,timeFilter);
		String drillDate="";
		
		if (headerCompleted != null && trailerCompleted == null) {
			return "In Progress Now";
		}
		else if (headerCompleted == null) {
			return "N/A";
		}
		
		drillDate=trailerCompleted[trailerCompleted.length-1].getBeginDate();
		return (drillDate.substring(6, 8) + "/" + drillDate.substring(4, 6) + "/" + drillDate.substring(0, 4));
	}
	
	public int getNumberExecutionAborted(String uprocFilter){
		int aborted=getNumberExecutionCompleted(uprocFilter,"ABORTED","*","*");
		return aborted;
	}

	public void closeService() throws DuwsException, RemoteException {
		service.logout(ctxHolder.getToken());		
	}


	public List<String> getSessionUProcs(String session) throws DuwsException, SessionTimedOutException, RemoteException {
		List<String> list = new ArrayList<String>();
		
		SessionFilter sf = new SessionFilter(session);
		try {
			SessionItem[] sits = service.getListSession(ctxHolder, sf);
			if (sits!=null) {
				SessionTree st = service.getSessionTree(ctxHolder, sits[0]);
				SessionNode nd = st.getRoot();
				list.add(nd.getUprocName());
				addChildren(list, nd);
			}
		}
		catch (Exception e) { e.printStackTrace(); }
		
		return list;
	}	
	
	private void addChildren(List<String> list, SessionNode nd) throws DuwsException, SessionTimedOutException, RemoteException {
		SessionNode[] nodes = nd.getChildOK();
		if (nodes!=null)
			for (SessionNode n : nodes) {
				list.add(n.getUprocName());				
				addChildren(list, n);
			}
		
		nodes = nd.getChildKO();
		if (nodes!=null)
			for (SessionNode n : nodes) {
				list.add(n.getUprocName());
				addChildren(list, n);
			}
	}


	public List<ExecutionItem> getLastExecutions(String sessionFO) {
		ArrayList<ExecutionItem> list = new ArrayList<ExecutionItem>(); 
		
		ExecutionItem[] execList = null;
		ExecutionFilter filterCompleted = new ExecutionFilter();
		
		filterCompleted.setUproc("*");
		filterCompleted.setStatus("COMPLETED");
		filterCompleted.setSession(sessionFO);

		try {
			execList=service.getListExecution(ctxHolder, filterCompleted);
			
			if (execList!=null) {
				int maxSessionLaunchId = 0;
				for (ExecutionItem item : execList) 
					maxSessionLaunchId = Math.max(maxSessionLaunchId, Integer.parseInt(item.getIdent().getNumSess()));
				
				for (ExecutionItem item : execList) 
					if (maxSessionLaunchId == Integer.parseInt(item.getIdent().getNumSess()))
						list.add(item);				
			}
		}
		catch (Exception e)	{
			e.printStackTrace();
		}
		return list;
	}
}
