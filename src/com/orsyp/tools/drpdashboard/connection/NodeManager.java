package com.orsyp.tools.drpdashboard.connection;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.servlet.ServletContext;

import com.orsyp.tools.drpdashboard.model.Node;
import com.orsyp.tools.drpdashboard.model.Uvms;


public class NodeManager {
	
	private static HashMap<String,Node> nodes = new HashMap<String,Node>();	
	private static Uvms uvms = null;
	private static Node refNode = null;
	private static ServletContext ctx = null;

	public static Collection<Node> getNodes(ServletContext servletContext) throws IOException {
		ctx = servletContext;
		if (uvms==null)
			loadData();
		
		return nodes.values();
	}
	
	public static Node getNode(ServletContext servletContext, String node) throws IOException {
		ctx = servletContext;
		if (uvms==null)
			loadData();

		if (!nodes.containsKey(node)) {
			Node n = new Node(uvms,node);
			n.isReferenceNode = false;
			nodes.put(node, n);
		}
		
		return nodes.get(node);
	}
	
	public static Node getRefNode(ServletContext servletContext) throws IOException {
		ctx = servletContext;
		if (uvms==null)
			loadData();
		
		return refNode;
	}
	
	public static Uvms getUvms(ServletContext servletContext) throws IOException {
		ctx = servletContext;
		if (uvms==null)
			loadData();
		
		return uvms;
	}
	
	public static Uvms getCurrentUvms() throws IOException {
		return uvms;
	}
	
	
	private static void loadData() throws FileNotFoundException, IOException {		
		uvms=loadSerializedData();		
		if (uvms==null)
			return;
		
		Node n = new Node(uvms,uvms.refNodeName);
		n.isReferenceNode = true;
		nodes.put(uvms.refNodeName, n);
		refNode=n;		
				
		/*
		//load uvms and reference node connection data from text file
		Properties p = new Properties();
		p.load(new FileReader(servletContext.getRealPath("")+File.separator+"WEB-INF/config/uvms.conf"));
		uvms = new Uvms();
		uvms.url = p.getProperty("url");
		uvms.uvmsHost = p.getProperty("server");
		uvms.uvmsPort = Integer.parseInt(p.getProperty("port"));
		uvms.uvmsLogin = p.getProperty("user");
		uvms.uvmsPass = p.getProperty("password");
		uvms.company = p.getProperty("company");
		uvms.area = p.getProperty("area");
		
		String refNodeName = p.getProperty("node");
		
		Node n = new Node(uvms,refNodeName);
		n.isReferenceNode = true;
		nodes.put(refNodeName, n);
		refNode=n;
		*/		
	}

	private static Uvms loadSerializedData() {
		String filename = ctx.getRealPath("")+File.separator+"WEB-INF/config/conn";
		if (!(new File(filename).exists())) {
			System.out.println("Connection config file not found: "+filename);
			return null;
		}
		System.out.println("Loading connection config file '"+filename+"' ...");
		try {
			FileInputStream fos = new FileInputStream(filename);
			GZIPInputStream gz = new GZIPInputStream(fos);
			BufferedInputStream buf = new BufferedInputStream(gz);
			
			ObjectInputStream oos = new ObjectInputStream(buf);			
			return (Uvms) oos.readObject();					
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return null;	
	}
	
	public static void saveConf(Uvms newUvms, ServletContext servletContext) throws FileNotFoundException, IOException {
		uvms = newUvms;
		ctx=servletContext;
		String filename = ctx.getRealPath("")+File.separator+"WEB-INF/config/conn";
		try {
			FileOutputStream fo = new FileOutputStream( new File(filename));
			GZIPOutputStream gz = new GZIPOutputStream(fo);					   
			BufferedOutputStream bf = new BufferedOutputStream(gz);
			ObjectOutputStream oout = new ObjectOutputStream(bf);			
			oout.writeObject(uvms);
			oout.close();
		} catch (Exception e) {
			e.printStackTrace();			
		}	
		loadData();
	}
}

