<%@page import="com.orsyp.tools.drpdashboard.connection.NodeManager"%>
<%@page import="com.orsyp.tools.drpdashboard.AppManager"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" 
    import="java.util.*"
    import="com.orsyp.tools.drpdashboard.model.*"   
    import="com.orsyp.tools.drpdashboard.AppManager"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ORSYP Migration Tools - User Administration</title>
<link href="css/table.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">	
	window.onload = function(){	
		showbox();
	};

	function showbox() {
		
		document.getElementById('btsubmit').value = 'Add';
		document.getElementById('boxtitleapp').innerHTML = 'Add new application';				
		document.getElementById("f_name").value='';
		document.getElementById("f_duration").value='';
		document.getElementById("f_fosession").value='';
		document.getElementById("f_fonodes").value='';
		document.getElementById("f_fbsession").value='';
		document.getElementById("f_fbnodes").value='';
		document.getElementById("f_jobloguproc").value='';
		document.getElementById("f_joblogline").value='';		
		
		document.getElementById('btsubmit').disabled=false;				
	};
	
	function editRow(name, duration, fosession,fonodes, fbsession, fbnodes,jobloguproc,joblogline) {		
		document.getElementById("f_name").value=name;
		document.getElementById("f_duration").value=duration;
		document.getElementById("f_fosession").value=fosession;
		document.getElementById("f_fonodes").value=fonodes;
		document.getElementById("f_fbsession").value=fbsession;
		document.getElementById("f_fbnodes").value=fbnodes;
		document.getElementById("f_jobloguproc").value=jobloguproc;
		document.getElementById("f_joblogline").value=joblogline;
		
		document.getElementById('btsubmit').value = 'Modify';
		document.getElementById('boxtitleapp').innerHTML = 'Edit application';
		document.getElementById('btsubmit').disabled=false;
			
		scroll(0,0);	
	}

	function stopRKey(evt) {
		var evt = (evt) ? evt : ((event) ? event : null);
		var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
		if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
	}

	document.onkeypress = stopRKey; 	
</script>
</head>
<body>
	<div style="width: 1000px; margin: 0 auto;">
	<div style="float:left; padding-left:250px">
		<img alt="" src="../images/logo.jpg">
	</div>
	
<%
	Uvms uvms = NodeManager.getCurrentUvms();
	String area ="";	
	String company ="";
	String url ="";
	String node ="";
	String uvmsPort ="";
	String uvmsHost ="";
	String uvmsLogin ="";
	String uvmsPass ="";
	
	if (uvms!=null) {
		area =uvms.area;	
		company =uvms.company;
		url =uvms.url;
		node =uvms.refNodeName;
		uvmsPort = ""+uvms.uvmsPort;
		uvmsHost =uvms.uvmsHost;
		uvmsLogin =uvms.uvmsLogin;
		uvmsPass =uvms.uvmsPass;
	}
%>	
	
	<div class="formbox">
		<form class="myForm"  method="post" action="../SaveUvms">
		<fieldset>
			<legend class="boxtitle">
				$Universe Connection Configuration
			</legend>					
			<label>
				Server					
				<input type="text" name="server" id="f_server" value="<%=uvmsHost%>"/>
			</label>
			<label>
				Port
				<input type="text" name="port" id="f_port" value="<%= uvmsPort%>"/>							
			</label>
			<label class="first">
				Web service url <small><i>(Ex: http://localhost:8080/du_web_services_6.2.01_all_os/DuwsSEI?wsdl)</i></small> 	
				<input type="text" style="width:500px" name="url" id="f_url" value="<%= url%>"/>			
			</label>
			<label>
				User
				<input type="text" name="user" id="f_user" value="<%= uvmsLogin%>"/>							
			</label>			
			<label>
				Password
				<input type="password" name="password" id="f_password" value="<%= uvmsPass%>"/>			
			</label>			
			<label>
				Company
				<input type="text"  name="company" id="f_company" value="<%= company%>"/>			
			</label>
			<label>
				Reference node
				<input type="text"  name="node" id="f_node" value="<%= node%>"/>							
			</label>
			<label>
				Area
				<input type="text"  name="area" id="f_area" value="<%= area%>"/>							
			</label>			
		</fieldset>		
		<input type="submit" class="button" id="btsubmitevms" name="submit" onsubmit="this.disabled=true;" value="Save" />
		</form>

	</div>	
	
	<div class="formbox">
		<form class="myForm" name="form_flussi" method="post" action="../EditApp">
		<input type="hidden" name="id" id="f_id"/>
		<fieldset>
			<legend class="boxtitle" id="boxtitleapp">
				Insert application
			</legend>		
			<label class="first">
				App name
				<input type="text" style="width:250px" name="name" id="f_name" />			
			</label>
			<label>
				Duration					
				<input type="text" name="duration" id="f_duration"/>
			</label>
			<label>
				JobLog UProc
				<input type="text" style="width:200px" name="jobloguproc" id="f_jobloguproc"/>							
			</label>
			<label>
				JobLog Line
				<input type="text" style="width:100px" name="joblogline" id="f_joblogline"/>							
			</label>			
			<label>
				Failover Session
				<input type="text" style="width:250px" name="fosession" id="f_fosession"/>			
			</label>			
			<label>
				Failover Nodes
				<input type="text" style="width:500px" name="fonodes" id="f_fonodes"/>			
			</label>
			<label>
				Failback Session
				<input type="text" style="width:250px" name="fbsession" id="f_fbsession"/>							
			</label>
			<label>
				Failback Nodes
				<input type="text" style="width:500px" name="fbnodes" id="f_fbnodes"/>							
			</label>			
		</fieldset>		
		<input type="submit" class="button" id="btsubmit" name="submit" onsubmit="this.disabled=true;" value="" />
		<input type="button" class="button" value="Cancel" onclick="showbox();">			
		</form>

	</div>	
	
<% 
	Collection<App> list = AppManager.getData();
	if (list.size()>0) {
%>	
	
	<table id="mytable" cellspacing="0" width="1000px"><tr>
			    	
		<th>App name</th>
		<th>Duration</th>
		<th>Failover Session</th>
		<th>Failover Nodes</th>
		<th>Failback Session</th>
		<th>Failback Nodes</th>
		<th>JobLog UProc</th>
		<th>LobLog line</th>
		<th width="40px"></th>
		</tr>
		
<%					
		int i=0;
		if (list!=null)
			for (App app : list) {
				String classinfo = "";
				if ((i % 2) != 1) 
					classinfo = "class=\"alt\"";
				String onclick ="onclick=\"editRow('"+app.applicationName+"','"+
													app.duration+"','"+
													app.sessionFO+"','"+
													app.getStringList(app.nodesFO)+"','"+
													app.sessionFB+"','"+
													app.getStringList(app.nodesFB)+"','"+
													app.joblogUproc+"','"+
													app.joblogLine+"');\"";
%>													
			    <tr <%=classinfo%>>			 
				<td <%=onclick%>><%=app.applicationName%></td>
		        <td <%=onclick%>><%=app.duration%></td>
		        <td <%=onclick%>><%=app.sessionFO%></td>
		        <td <%=onclick%>><%= app.getStringList(app.nodesFO)%></td>
		        <td <%=onclick%>><%=app.sessionFB%></td>
		        <td <%=onclick%>><%=app.getStringList(app.nodesFB)%></td>
		        <td <%=onclick%>><%=app.joblogUproc%></td>
		        <td <%=onclick%>><%=app.joblogLine%></td>
		        <td width="19px"><a href="#" title="Edit app" <%=onclick%>><img src="img/edit.png" alt="Mod"/></a>
		         				 <a href="../DeleteApp?id=<%=app.applicationName%>" title="Delete app"><img src="img/delete.png" alt="Canc"/></a></td>			
			    </tr>
<%		    
				i++;
			}
	}
%>		
	</table>
	
<%
	if (uvms!=null) {
%>
	<br>
	<p align=center>
		<a href="../dashboard.jsp"><img alt="" src="../images/data.png"><br>See applications status</a>
	</p>
<%		
	}
%>
	
	
	</div>
</body>
</html>