<%@ page language="java" import="com.orsyp.tools.drpdashboard.AppManager, com.orsyp.tools.drpdashboard.model.*, java.util.*"%>
<html>
<head>
<LINK REL="SHORTCUT ICON" HREF="images/dashboard.bmp">
<title>Dollar Universe V6 Dashboards</title>
<meta name="Dashboard" content="Dollar Universe V6 Dashboards">
<meta HTTP-EQUIV="refresh" CONTENT="60">
<link rel="stylesheet" href="css/style.css" type="text/css" media="print, projection, screen" />
<link rel="stylesheet" href="css/table.css" type="text/css" media="print, projection, screen" />
</head>
<body>
<div class="maindiv" align="center">
	<img src="images/logo.jpg" border="0" alt="logo" height="80px" width="400px" align="center"/>
	<br>
	<br>
	<table id="maintable" class="tablesorter">
		<thead>
			<tr>
				<th>Application</th>
				<th colspan="2">Failover Process</th>
				<th>Duration</th>
				<th colspan="2">RTO</th>
				<th colspan="2">Failback Process</th>
				<th>Duration</th>
				<th colspan="2">RTO</th>
				<th>DR Readiness</th>
				<th>Last Drill Date</th>
				<!-- <th>Reset</th> -->
			</tr>
		</thead>
		<tbody>
<%
Collection<App> apps = AppManager.getData();
int idx=0;
for (App app : apps) {
%>
			<tr>
				<td><%=app.applicationName%></td>
<%
	if (app.data==null) {
%>
				<td colspan="12">Unable to get data</td>
<%
	} else {
%>								
				<td>
					<div id="progress">    				
    					<div id="bar" style="width: <%=app.data.percentagesFO%>%"></div>
					</div>
				</td>				
				<td><%= app.data.percentagesFO%>%&nbsp;finished</td>
				<td class="centth"><%= app.data.durationFO %></td>
				<td class="centth"><%= app.data.uprocsFO.size() %>&nbsp;steps</td>
				<td class="centth"><%= app.duration %></td>
				<td>
					<div id="progress">    				
    					<div id="bar" style="width: <%=app.data.percentagesFB%>%"></div>
					</div>
				</td>
				<td><%= app.data.percentagesFB %>%&nbsp;finished</td>
				<td class="centth"><%= app.data.durationFB %></td>
				<td class="centth"><%= app.data.uprocsFB.size() %>&nbsp;steps</td>
				<td class="centth"><%= app.duration %></td>
				
				<td class="centth">
<%	
		if (app.data.drReadiness) {
%>				
					<img src="images/thumbsdown.png" alt="ME" align="center" height="26" width="32" />Impaired
<%
		} else {
%>							
					<img src="images/thumbsup.png" alt="ME" align="center" height="26" width="32" />Ready
<%	
		}	
%>							
				</td>
				<td class="centth"><%= app.data.drillDateFO %> - <%= app.data.drillDateFB %></td>
				<!-- <td><form action="reset.jsp" method="post"><input type="hidden" name="appName" value="<%=app.applicationName%>"><button type="submit">Reset</button></form> -->
			</tr>
<%
	}
	idx++;
}
%>
		</tbody>
	</table>
	
	<br>
	<br>
	<a href="admin/admin.jsp"><img src="images/configure.png"/><br>Configure</a>
	<br>
	<br>
					
	<form action="dashboard.jsp" method="post">
		<small>The page automatically refreshes itself every 60 seconds</small>
		<input type="submit" value="Refresh now" name="Submit" id="frm1_submit" />
	</form>
		
</div>
</body>
</html>